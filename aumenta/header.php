<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();
?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<script type="text/javascript">
	var customPath = "<?=get_stylesheet_directory_uri();?>/fonts";
	</script>
	<script type="text/javascript" src="<?=get_stylesheet_directory_uri();?>/js/UVG.js" async></script>
	
	<!-- Inicia código pegado por Gerber Barillas -->
	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K4D9D86');</script>
	<!-- End Google Tag Manager -->

	<!-- final de código pegado por Gerber Barillas -->
	

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4D9D86"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php
	$product_tour_enabled = et_builder_is_product_tour_enabled();
	$page_container_style = $product_tour_enabled ? ' style="padding-top: 0px;"' : ''; ?>
	<div id="page-container"<?php echo $page_container_style; ?>>
<?php
	if ( $product_tour_enabled || is_page_template( 'page-template-blank.php' ) ) {
		return;
	}

	$et_secondary_nav_items = et_divi_get_top_nav_items();

	$et_phone_number = $et_secondary_nav_items->phone_number;

	$et_email = $et_secondary_nav_items->email;

	$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;

	$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;

	$et_secondary_nav = $et_secondary_nav_items->secondary_nav;

	$et_top_info_defined = $et_secondary_nav_items->top_info_defined;

	$et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option( 'header_style', 'left' ) ? true : false;
?>

	<?php if ( $et_top_info_defined && ! $et_slide_header || is_customize_preview() ) : ?>
		<?php ob_start(); ?>
		<div id="top-header"<?php echo $et_top_info_defined ? '' : 'style="display: none;"'; ?>>
			<div class="container clearfix">

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo et_sanitize_html_input_text( $et_phone_number ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
				<?php endif; ?>

				<?php
				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				} ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>

				<div id="et-secondary-menu">
				<?php
					if ( ! $et_contact_info_defined && true === $show_header_social_icons ) {
						get_template_part( 'includes/social_icons', 'header' );
					} else if ( $et_contact_info_defined && true === $show_header_social_icons ) {
						ob_start();

						get_template_part( 'includes/social_icons', 'header' );

						$duplicate_social_icons = ob_get_contents();

						ob_end_clean();

						printf(
							'<div class="et_duplicate_social_icons">
								%1$s
							</div>',
							$duplicate_social_icons
						);
					}

					if ( '' !== $et_secondary_nav ) {
						echo $et_secondary_nav;
					}

					et_show_cart_total();
				?>
				</div> <!-- #et-secondary-menu -->

			</div> <!-- .container -->
		</div> <!-- #top-header -->
	<?php
		$top_header = ob_get_clean();

		/**
		 * Filters the HTML output for the top header.
		 *
		 * @since ??
		 *
		 * @param string $top_header
		 */
		echo apply_filters( 'et_html_top_header', $top_header );
	?>
	<?php endif; // true ==== $et_top_info_defined ?>

	<?php if ( $et_slide_header || is_customize_preview() ) : ?>
		<?php ob_start(); ?>
		<div class="et_slide_in_menu_container">
			<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) || is_customize_preview() ) { ?>
				<span class="mobile_menu_bar et_toggle_fullscreen_menu"></span>
			<?php } ?>

			<?php
				if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
					<div class="et_slide_menu_top">

					<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
						<div class="et_pb_top_menu_inner">
					<?php } ?>
			<?php }

				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				}

				et_show_cart_total();
			?>
			<?php if ( false !== et_get_option( 'show_search_icon', true ) || is_customize_preview() ) : ?>
				<?php if ( 'fullscreen' !== et_get_option( 'header_style', 'left' ) ) { ?>
					<div class="clear"></div>
				<?php } ?>
				<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);
					?>
					<button type="submit" id="searchsubmit_header"></button>
				</form>
			<?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo et_sanitize_html_input_text( $et_phone_number ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
				<?php endif; ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>
			<?php if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
				<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
					</div> <!-- .et_pb_top_menu_inner -->
				<?php } ?>

				</div> <!-- .et_slide_menu_top -->
			<?php } ?>

			<div class="et_pb_fullscreen_nav_container">
				<?php
					$slide_nav = '';
					$slide_menu_class = 'et_mobile_menu';

					$slide_nav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
					$slide_nav .= wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
				?>

				<ul id="mobile_menu_slide" class="<?php echo esc_attr( $slide_menu_class ); ?>">

				<?php
					if ( '' == $slide_nav ) :
				?>
						<?php if ( 'on' == et_get_option( 'divi_home_link' ) ) { ?>
							<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
						<?php }; ?>

						<?php show_page_menu( $slide_menu_class, false, false ); ?>
						<?php show_categories_menu( $slide_menu_class, false ); ?>
				<?php
					else :
						echo( $slide_nav );
					endif;
				?>

				</ul>
			</div>
		</div>
	<?php
		$slide_header = ob_get_clean();

		/**
		 * Filters the HTML output for the slide header.
		 *
		 * @since ??
		 *
		 * @param string $top_header
		 */
		echo apply_filters( 'et_html_slide_header', $slide_header );
	?>
	<?php endif; // true ==== $et_slide_header ?>

	<?php ob_start(); ?>
		<header id="main-header" data-height-onload="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>">
			<!-- Academico menu -->
			<div id="menu-academico">
				<div class="academico-sub">
					<div class="first-step">
						<div class="col1">
							<h2>
								ESTUDIA EN UVG
							</h2>
							<a href="javascript:void(0);" class="por-grado-click">
								POR GRADO <i class="fa fa-chevron-right" aria-hidden="true"></i>
							</a>
							<a href="javascript:void(0);" class="por-facultad-click">
								POR FACULTAD <i class="fa fa-chevron-right" aria-hidden="true"></i>
							</a>
						</div>
						<div class="col2">
							<div class="menu">
								<?php wp_nav_menu( array('menu'=>1354) );?>
							</div>
						</div>
						<div class="col3">
							<?php if ( is_active_sidebar( 'academico_menu2' ) ) : ?>
								<?php dynamic_sidebar( 'academico_menu2' ); ?>
							<?php endif; ?>
						</div>
						<div class="por-grado">
							<?php wp_nav_menu( array('menu'=>1355) );?>
						</div>
						<div class="por-facultad">
							<?php wp_nav_menu( array('menu'=>1645) );?>
						</div>
					</div>
				</div>
			</div>
			<!-- Academico menu -->

            <!--Quick access menus-->
            <div class="quick-menus">
                <a class="menu_close">Cerrar X</a>
                <div id="estudiantes" class="qmenu">
                    <?php wp_nav_menu( array('menu'=>3) );?>
                </div>

                <div id="colaboradores" class="qmenu">
                    <?php wp_nav_menu( array('menu'=>4) );?>
                </div>

                <div id="egresados" class="qmenu">
                    <?php wp_nav_menu( array('menu'=>5) );?>
                </div>

                <div id="socios" class="qmenu">
                    <?php wp_nav_menu( array('menu'=>6) );?>
                </div>

                <div id="padres" class="qmenu">
                    <?php wp_nav_menu( array('menu'=>7) );?>
                </div>
            </div>
            <!--Quick access menus-->
            <!--Egresados listado-->
			<div class="egresados-general">
				<ul></ul>
			</div>
            <!--Widgets for the images-->
			<div class="widgets">
				<div id="wd-nosotros">
					<?php if ( is_active_sidebar( 'nosotros_menu' ) ) : ?>
						<?php dynamic_sidebar( 'nosotros_menu' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-nosotros2">
					<?php if ( is_active_sidebar( 'nosotros_menu2' ) ) : ?>
						<?php dynamic_sidebar( 'nosotros_menu2' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-admisiones">
					<?php if ( is_active_sidebar( 'admisiones_menu' ) ) : ?>
						<?php dynamic_sidebar( 'admisiones_menu' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-admisiones2">
					<?php if ( is_active_sidebar( 'admisiones_menu2' ) ) : ?>
						<?php dynamic_sidebar( 'admisiones_menu2' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-academico_menu">
					<?php if ( is_active_sidebar( 'academico_menu' ) ) : ?>
						<?php dynamic_sidebar( 'academico_menu' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-academico_menu2">
					<?php if ( is_active_sidebar( 'academico_menu2' ) ) : ?>
						<?php dynamic_sidebar( 'academico_menu2' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-vida">
					<?php if ( is_active_sidebar( 'vida_menu' ) ) : ?>
						<?php dynamic_sidebar( 'vida_menu' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-vida2">
					<?php if ( is_active_sidebar( 'vida_menu2' ) ) : ?>
						<?php dynamic_sidebar( 'vida_menu2' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-impacto">
					<?php if ( is_active_sidebar( 'impacto_menu' ) ) : ?>
						<?php dynamic_sidebar( 'impacto_menu' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-impacto2">
					<?php if ( is_active_sidebar( 'impacto_menu2' ) ) : ?>
						<?php dynamic_sidebar( 'impacto_menu2' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-investigacion">
					<?php if ( is_active_sidebar( 'investigacion_menu' ) ) : ?>
						<?php dynamic_sidebar( 'investigacion_menu' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-investigacion2">
					<?php if ( is_active_sidebar( 'investigacion_menu2' ) ) : ?>
						<?php dynamic_sidebar( 'investigacion_menu2' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-servicios">
					<?php if ( is_active_sidebar( 'servicios_menu' ) ) : ?>
						<?php dynamic_sidebar( 'servicios_menu' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-servicios2">
					<?php if ( is_active_sidebar( 'servicios_menu2' ) ) : ?>
						<?php dynamic_sidebar( 'servicios_menu2' ); ?>
					<?php endif; ?>
				</div>
				<div id="wd-proxima">
					<?php if ( is_active_sidebar( 'proxima_menu' ) ) : ?>
						<?php dynamic_sidebar( 'proxima_menu' ); ?>
					<?php endif; ?>
				</div>
			</div>

			<div class="container clearfix et_menu_container">
			<?php
				$logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
					? $user_logo
					: $template_directory_uri . '/images/logo.png';

				ob_start();
			?>
				<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" />
					</a>
				</div>
			<?php
				$logo_container = ob_get_clean();

				/**
				 * Filters the HTML output for the logo container.
				 *
				 * @since ??
				 *
				 * @param string $logo_container
				 */
				echo apply_filters( 'et_html_logo_container', $logo_container );
            ?>
                <div id="header-buttons" data-height="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>" data-fixed-height="<?php echo esc_attr( et_get_option( 'minimized_menu_height', '40' ) ); ?>">
                    <!--Quick access buttons-->
                    <div id="q-buttons">
                        <a href="#" id="estudiantesb" data-id="estudiantes">
                            ESTUDIANTES
                        </a>
                        <a href="#" id="colaboradoresb" data-id="colaboradores">
                            COLABORADORES
                        </a>
                        <a href="#" id="egresadosb" data-id="egresados">
                            EGRESADOS
                        </a>
                        <a href="#" id="sociosb" data-id="socios">
                            SOCIOS
                        </a>
                        <a href="#" id="padresb" data-id="padres">
                            PADRES
                        </a>
                    </div>
                    <!--Quick access buttons-->
                    
                    <!--Header buttons-->
                    <div id="buttons">
                        <div class="search">
                            <script>
                            (function() {
                                var cx = '006788646225704591890:oidgc6zgvu8';
                                var gcse = document.createElement('script');
                                gcse.type = 'text/javascript';
                                gcse.async = true;
                                gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                                var s = document.getElementsByTagName('script')[0];
                                s.parentNode.insertBefore(gcse, s);
                            })();
                            </script>
                            <gcse:search></gcse:search>
						</div>
						<div class="header-buttons-container">
							<a href="http://uvg.edu.gt/educacion-continua" class="educacion-continua">
								<img src="https://res.cloudinary.com/webuvg/image/upload/v1541710893/WEB/Home/logo-educon2x.png">
							</a>
							<a href="http://noticias.uvg.gt/" class="actualidad-uvg">
								<img src="https://res.cloudinary.com/webuvg/image/upload/v1541710893/WEB/Home/logo-actualidaduvg2x.png" alt="">
							</a>
						</div>
                    </div>
                    <!--Header buttons-->

				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);
					?>
					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
			<div class="main-menu">
				<?php wp_nav_menu( array('menu'=>2) );?>
				<ul class="icons-header">
					<li>
						<a href="https://www.facebook.com/universidaddelvallegt/" target="_blank">
							<i class="fa fa-facebook-square" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/uvggt" target="_blank">
							<i class="fa fa-twitter-square" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="http://www.youtube.com/user/MediaUVG" target="_blank">
							<i class="fa fa-youtube-square" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://www.instagram.com/uvggt/" target="_blank">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://www.linkedin.com/company/universidad-del-valle-de-guatemala/" target="_blank">
							<i class="fa fa-linkedin-square" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://www.pinterest.com/universidaddelvalledeguatemala/" target="_blank">
							<i class="fa fa-pinterest-square" aria-hidden="true"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="main-menu-mobile">
				<div class="bread">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</div>
				<?php wp_nav_menu( array('menu'=>2) );?>
			</div>
		</header> <!-- #main-header -->
	<?php
		$main_header = ob_get_clean();

		/**
		 * Filters the HTML output for the main header.
		 *
		 * @since ??
		 *
		 * @param string $main_header
		 */
		echo apply_filters( 'et_html_main_header', $main_header );
	?>
		<div id="et-main-area">
	<?php
		/**
		 * Fires after the header, before the main content is output.
		 *
		 * @since ??
		 */
		do_action( 'et_before_main_content' );
