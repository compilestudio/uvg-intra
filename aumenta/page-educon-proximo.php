<?php
/*
 * Template Name: Educones proximos
*/
get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>
			
			<!-- Aumenta plantilla aqui -->
			<div class="et_builder_inner_content et_pb_gutters3">
				<div class="et_pb_section et_pb_section_0 et_section_regular" style="padding-top: 0;">
					<div class="et_pb_row et_pb_row_0 educon_fix">
						<!-- Educones-->
						<div id="educones">
							<!--Search-->
							<div class="search_educon">
								<div class="search">
									<input type="text" placeholder="BUSCAR" id="search_educon">
									<button type="button">
										<i class="fa fa-search" aria-hidden="true"></i>
									</button>
								</div>
							</div>
							<!--Search-->
							<ul class="list educones">
								<?php
									$posts = get_posts( 
										array( 
											'post_type' => 'educon',
											'posts_per_page' => 200
										)
									);
									// var_dump($posts);
									foreach($posts as &$p):
										$post_date = new DateTime(get_field('fecha',$p->ID));
										$current_date = new DateTime();
										if ($post_date >= $current_date):
									?>
										<li class="element">
											<h2 class="title">
												<?=$p->post_title;?>
											</h2>
											<div class="sub">
												<div class="fecha">
													<?php
													$date = get_field('fecha',$p->ID);
													$date = $date[6].$date[7]."/".$date[4].$date[5]."/".$date[0].$date[1].$date[2].$date[3];
													echo $date;?>
													-
												</div>
												<div class="lugar">
													<?=get_field('lugar',$p->ID);?>
												</div>
											</div>
											<div class="text">
												<div class="image">
													<img src="<?=get_field('imagen',$p->ID);?>" alt="<?=$p->post_title;?>">
													<div class="inversion">
														<strong>INVERSIÓN: </strong> Q<?=get_field('inversion',$p->ID);?>
													</div>
												</div>
												<div class="right">
													<div class="descripcion">
														<strong>DESCRIPCIÓN:</strong>
														<?=wpautop(substr($p->post_content,0,300));?>
													</div>
													<div class="dirigido">
														<strong>DIRIGIDO A:</strong>
														<?=wpautop(substr(get_field('dirigido_a',$p->ID),0,300));?>
													</div>
												</div>
											</div>
											<div class="bottom-links">
												<?php $afiche = get_field('afiche',$p->ID);
												if(!empty($afiche)):?>
													<a href="<?=$afiche;?>" target="_blank">
														DESCARGA AFICHE
													</a>
												<?php endif;?>
												<?php $info = get_field('info_link',$p->ID);
												if(!empty($info)):?>
													<a href="<?=$info;?>">
														+INFO
													</a>
												<?php endif;?>
												<?php $contacto =get_field('contacto',$p->ID);
												if(!empty($contacto)):?>
													<a href="<?=$contacto;?>">
														CONTACTO
													</a>
												<?php endif;?>
												<?php $cv = get_field('cv_del_ponente',$p->ID);
												if(!empty($cv)):?>
													<a href="<?=$cv;?>" target="_blank">
														CV DEL PONENTE
													</a>
												<?php endif;?>
												<?php $reg = get_field('registrate',$p->ID);
												if(!empty($reg)):?>
													<a href="<?=$reg;?>" target="_blank">
														REGISTRATE
													</a>
												<?php endif;?>
											</div>
										</li>
									<?php endif;
									endforeach;?>
							</ul>
							<ul class="pagination"></ul>
						</div>
						<!-- Educones-->
						<div class="educones-proximos">
							<a href="/beta/educones-anteriores/">
								Ir a EDUCONES anteriores
							</a>
						</div>

						<div class="suscribirte-educones">
							<div class="logo-educon">
								<img src="<?=get_stylesheet_directory_uri();?>/images/logo-educon.png" alt="">
							</div>
							<div class="formulario">
								<div class="title">
									¿Te gustaría enterarte de los EDUCON?
								</div>
								<div class="formulario">
									<div class="_form_397"></div><script src="https://uvg.activehosted.com/f/embed.php?id=397" type="text/javascript" charset="utf-8"></script>
								</div>
							</div>
						</div>


					</div> <!-- .et_pb_row -->				
				</div> <!-- .et_pb_section -->			
			</div>
			<!-- Aumenta plantilla aqui -->


<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
