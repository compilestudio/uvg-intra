<?php
/*
 * Template Name: Directorio
 * Template Post Type: nosotros
*/
$aurl = get_stylesheet_directory_uri();
$surl = get_site_url();

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

<!-- Aumenta plantilla aqui -->
	<div class="directorio-container">
		<div class="search">
			<input type="text" name="search" id="search" placeholder="Escribe el nombre de la persona que quieres buscar">
			<button type="button">
				<i class="fa fa-search"></i> Buscar
			</button>
		</div>
		<?php 
			$terms = get_terms( 'departamento', array(
				'hide_empty' => false,
				'parent' => 0
			));
		?>
		<div class="filters-container">
			<ul class="filters">
			<?php 
				for($i=0;$i<ceil(count($terms)/2);$i++):
				?>
				<li>
					<a href="#entries" data-id="<?=$terms[$i]->term_id;?>">
						<?=$terms[$i]->name;?>
					</a>
				</li>
			<?php endfor;?>
			</ul>
			<ul class="filters">
			<?php 
				for($i=ceil(count($terms)/2);$i<count($terms);$i++):
				?>
				<li>
					<a href="#entries" data-id="<?=$terms[$i]->term_id;?>">
						<?=$terms[$i]->name;?>
					</a>
				</li>
			<?php endfor;?>
			</ul>
		</div>
		<!-- <div class="subtitle">
			<?=$terms[0]->name;?>
		</div> -->
		<div id="entries"></div>
		<div class="entries">
			<?php foreach($terms as $t):
				$sterms = get_terms( 'departamento', array(
					'hide_empty' => false,
					'parent' => $t->term_id
				));
			?>
				<div class="element fullwidth cat<?=$t->term_id;?>">
					<?=$t->name;?>
				</div>
				<?php
					$posts = get_posts( 
						array( 
							'post_type' => 'autoridades',
							'posts_per_page' => 200,
							'tax_query' => array( 
								array( 
									'taxonomy' => 'departamento', //or tag or custom taxonomy
									'field' => 'id', 
									'terms' => array($t->term_id) ,
									'include_children' => false
								)
							)
						)
					);
					foreach($posts as &$p):
						$color = get_field('color',$p->ID);
						if(!empty($color)):
							$color = "style='background: ".$color."'";
						endif;
						if(empty($color)):
							$color = get_field('color',"departamento_".$t->term_id);
							if(!empty($color)):
								$color = "style='background: ".$color."'";
							endif;
						endif;
					?>
				<div class="element cat<?=$t->term_id;?>">
					<h2 <?=$color;?>>
						<?=$p->post_title;?>
					</h2>
					<div class="text">
						<?=wpautop($p->post_content);?>
					</div>
				</div>
				<?php endforeach;?>
				
				<?php if(!empty($sterms)):
					foreach($sterms as $s):?>
					<div class="element fullwidth cat<?=$t->term_id;?>" style="margin-top: 20px;">
						<?=$s->name;?>
					</div>
					<?php
						$posts = get_posts( 
							array( 
								'post_type' => 'autoridades',
								'posts_per_page' => 200,
								'tax_query' => array( 
									array( 
										'taxonomy' => 'departamento', //or tag or custom taxonomy
										'field' => 'id', 
										'terms' => array($s->term_id),
										'include_children' => false
									)
								)
							)
						);
						foreach($posts as &$p):
							$color = get_field('color',$p->ID);
							if(!empty($color)):
								$color = "style='background: ".$color."'";
							endif;
							if(empty($color)):
								$color = get_field('color',"departamento_".$s->term_id);
								if(!empty($color)):
									$color = "style='background: ".$color."'";
								else:
									$color = get_field('color',"departamento_".$t->term_id);
									if(!empty($color)):
										$color = "style='background: ".$color."'";
									endif;
								endif;
							endif;
						?>
							<div class="element cat<?=$t->term_id;?>">
								<h2 <?=$color;?>>
									<?=$p->post_title;?>
								</h2>
								<div class="text">
									<?=wpautop($p->post_content);?>
								</div>
							</div>
						<?php endforeach;?>
				<?php endforeach;
				endif;?>
			<?php endforeach;?>
		</div>
	</div>
<!-- Aumenta plantilla aqui -->
</div> <!-- #main-content -->

<?php

get_footer();
