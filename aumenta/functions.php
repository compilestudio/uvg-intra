<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
        wp_enqueue_style( 'colorbox', trailingslashit( get_stylesheet_directory_uri() ) . 'css/colorbox.css', array(  ) );
        wp_enqueue_style( 'nivo', trailingslashit( get_stylesheet_directory_uri() ) . 'css/nivo-slider.css', array(  ) );
        wp_enqueue_style( 'gerber', trailingslashit( get_stylesheet_directory_uri() ) . 'gerber.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION

function add_theme_scripts() {
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/main.js', array ( 'jquery' ), 0.2, true);
    wp_enqueue_script( 'isotope-js', get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js', array(), '1.0.0', true );
    //http://listjs.com/
    wp_enqueue_script( 'list-js', get_stylesheet_directory_uri() . '/js/list.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'colorbox-js', get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'nivo-js', get_stylesheet_directory_uri() . '/js/jquery.nivo.slider.pack.js', array(), '1.0.0', true );
    wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/js/slick.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

//We add the custom post Types
function create_post_type() {
    register_post_type( 'carreras',
        array(
            'labels' => array(
                'name' => __( 'Carreras' ),
                'singular_name' => __( 'Carreras' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'nosotros',
        array(
            'labels' => array(
                'name' => __( 'Nosotros' ),
                'singular_name' => __( 'Nosotros' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'admisiones',
        array(
            'labels' => array(
                'name' => __( 'Admisiones' ),
                'singular_name' => __( 'Admisiones' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'academico',
        array(
            'labels' => array(
                'name' => __( 'Academico' ),
                'singular_name' => __( 'Academico' )
            ),
            'public' => true,
            'has_archive' => true
        )
    );

    register_post_type( 'vida',
        array(
            'labels' => array(
                'name' => __( 'Vida Estudiantil' ),
                'singular_name' => __( 'Vida Estudiantil' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'impacto',
        array(
            'labels' => array(
                'name' => __( 'Impacto' ),
                'singular_name' => __( 'Impacto' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'investigacion',
        array(
            'labels' => array(
                'name' => __( 'Investigación' ),
                'singular_name' => __( 'Investigación' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'servicios',
        array(
            'labels' => array(
                'name' => __( 'Servicios' ),
                'singular_name' => __( 'Servicio' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'facultades',
        array(
            'labels' => array(
                'name' => __( 'Facultades' ),
                'singular_name' => __( 'Facultades' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'uvgmaster',
        array(
            'labels' => array(
                'name' => __( 'UVG Masters' ),
                'singular_name' => __( 'UVG Masters' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
    
    // Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name' => _x( 'Facultad', 'taxonomy general name', 'textdomain' )
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'facultad' ),
	);

	register_taxonomy( 'facultad', 'egresados', $args );

    register_post_type( 'egresados',
        array(
            'labels' => array(
                'name' => __( 'Egresados' ),
                'singular_name' => __( 'Egresado' )
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array( 'facultad' )
        )
    );

    // Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name' => _x( 'Departamento', 'taxonomy general name', 'textdomain' )
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'departamento' ),
	);

	register_taxonomy( 'departamento', 'autoridades', $args );

    register_post_type( 'autoridades',
        array(
            'labels' => array(
                'name' => __( 'Autoridades' ),
                'singular_name' => __( 'Autoridad' )
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array( 'departamento' ),
        )
    );

    register_post_type( 'educon',
        array(
            'labels' => array(
                'name' => __( 'Educon' ),
                'singular_name' => __( 'Educon' )
            ),
            'public' => true,
            'has_archive' => true
        )
    );
    register_post_type( 'evento',
        array(
            'labels' => array(
                'name' => __( 'Eventos' ),
                'singular_name' => __( 'Evento' )
            ),
            'public' => true,
            'has_archive' => true
        )
    );
    register_post_type( 'aviso',
        array(
            'labels' => array(
                'name' => __( 'Avisos' ),
                'singular_name' => __( 'Aviso' )
            ),
            'public' => true,
            'has_archive' => true
        )
    );
}
  add_action( 'init', 'create_post_type' );

  add_action( 'admin_init', 'layers_add_post_menu_order' );
 
function layers_add_post_menu_order() {
    add_post_type_support( 'autoridades', 'page-attributes' );
    add_post_type_support( 'nosotros', 'page-attributes' );
    add_post_type_support( 'academico', 'revisions' );
    add_post_type_support( 'investigacion', 'revisions' );
    add_post_type_support( 'impacto', 'revisions' );
}

//Add Divi functionality to the custom post types
function my_et_builder_post_types( $post_types ) {
    $post_types[] = 'carreras';
    $post_types[] = 'nosotros';
    $post_types[] = 'admisiones';
    $post_types[] = 'academico';
    $post_types[] = 'vida';
    $post_types[] = 'impacto';
    $post_types[] = 'investigacion';
    $post_types[] = 'servicios';
    $post_types[] = 'facultades';
    $post_types[] = 'uvgmaster';
    
    return $post_types;
}
add_filter( 'et_builder_post_types', 'my_et_builder_post_types' );


function aumenta_widgets_init() {

	register_sidebar( array(
		'name'          => 'Nosotros Menu',
		'id'            => 'nosotros_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
    ) );
    
	register_sidebar( array(
		'name'          => 'Nosotros Menu Contacto',
		'id'            => 'nosotros_menu2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Admisiones Menu',
		'id'            => 'admisiones_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
    ) );
    
    register_sidebar( array(
		'name'          => 'Admisiones Menu Contacto',
		'id'            => 'admisiones_menu2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
    ) );
    
    register_sidebar( array(
		'name'          => 'Academico Menu',
		'id'            => 'academico_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Academico Menu Contacto',
		'id'            => 'academico_menu2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Vida Menu',
		'id'            => 'vida_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Vida Menu Contacto',
		'id'            => 'vida_menu2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Impacto Menu',
		'id'            => 'impacto_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Impacto Menu Contacto',
		'id'            => 'impacto_menu2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Investigación Menu',
		'id'            => 'investigacion_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Investigación Menu Contacto',
		'id'            => 'investigacion_menu2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Servicios Menu',
		'id'            => 'servicios_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Servicios Menu Contacto',
		'id'            => 'servicios_menu2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Próxima Fecha Exámen',
		'id'            => 'proxima_menu',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

}
add_action( 'widgets_init', 'aumenta_widgets_init' );


add_action( 'rest_api_init', 'relevanssi_rest_api_filter_add_filters' );

function relevanssi_rest_api_filter_add_filters() {
	//Aumenta
	//Registramos el api de egresados
	register_rest_route( 'egresados/v1', 'get', array(
		'methods'  => WP_REST_Server::READABLE,
		'callback' => 'egresados_callback',
	) );
	//Registramos el api de avisos
	register_rest_route( 'avisos/v1', 'get', array(
		'methods'  => WP_REST_Server::READABLE,
		'callback' => 'avisos_callback',
	) );
	//Registramos el api de eventos
	register_rest_route( 'eventos/v1', 'get', array(
		'methods'  => WP_REST_Server::READABLE,
		'callback' => 'eventos_callback',
	) );
}

//Api to get the posts
function egresados_callback( WP_REST_Request $data ) {
    $ops = $data->get_query_params();
    $cat = $ops["filter"];

    $params = array( 
        'post_type' => 'egresados',
        'posts_per_page' => 6,
        'orderby' => 'rand',
        'order'    => 'ASC'
    );
    if($cat!="*"){
        $params['tax_query'] = array( 
            array( 
                'taxonomy' => 'facultad', //or tag or custom taxonomy
                'field' => 'id', 
                'terms' => array($cat) 
            )
        );
    }

	$posts = get_posts( $params );
    
    foreach($posts as &$p):
        $p->link = get_field('youtube_link',$p->ID);
        $p->foto = get_field('foto',$p->ID);
        $p->cat = wp_get_post_terms($p->ID,'facultad')[0]->term_id;
    endforeach;

	$ret = $posts;

	return rest_ensure_response( $ret );
}
//Api to get the events
function eventos_callback( WP_REST_Request $data ) {
    $ops = $data->get_query_params();

    $params = array( 
        'post_type' => 'evento',
        'posts_per_page' => 50,
        'orderby' => 'post_date',
        'order'    => 'ASC'
    );

	$posts = get_posts( $params );
    
    $pr = [];
    foreach($posts as &$p):
        $p->fecha_inicio = get_field('fecha_inicio',$p->ID);
        $p->fecha_fin = get_field('fecha_fin',$p->ID);
        $p->hora_inicio = get_field('hora_inicio',$p->ID);
        $p->hora_fin = get_field('hora_fin',$p->ID);
        $p->ubicacion = get_field('ubicacion',$p->ID);
        $p->titulo_enlace = get_field('titulo_enlace',$p->ID);
        $p->enlace = get_field('enlace',$p->ID);
        $p->enlace_ver_mas = get_field('enlace_ver_mas',$p->ID);
        $p->titulo_enlace_ver_mas = get_field('titulo_enlace_ver_mas',$p->ID);

        $fi = substr($p->fecha_inicio,0,4)."-".substr($p->fecha_inicio,4,2)."-".substr($p->fecha_inicio,6,2)." 00:00:00.0";
        $fi = strtotime($fi);
        $ff = substr($p->fecha_fin,0,4)."-".substr($p->fecha_fin,4,2)."-".substr($p->fecha_fin,6,2)." 00:00:00.0";
        $ff = strtotime($ff);
        $p->fi = $fi;
        $p->ff =  $ff;

        $today = strtotime("today", time());

        if($fi>= $today){
            $pr[] = $p;
        }
    endforeach;

    // $ret = $posts;
    
    usort($pr, "cmp");

	$ret = $pr;

	return rest_ensure_response( $ret );
}
//Api to get the avisos
function avisos_callback( WP_REST_Request $data ) {
    $ops = $data->get_query_params();

    $params = array( 
        'post_type' => 'aviso',
        'posts_per_page' => 50,
        'orderby' => 'post_date',
        'order'    => 'ASC'
    );

	$posts = get_posts( $params );
    
    $pr = [];
    foreach($posts as &$p):
        $p->enlace_externo = get_field('enlace_externo',$p->ID);
        $p->fecha_inicio = get_field('fecha_inicio',$p->ID);
        $p->fecha_fin = get_field('fecha_fin',$p->ID);
        $p->titulo_enlace_ver_mas = get_field('titulo_enlace_ver_mas',$p->ID);
        $p->hora_inicio = get_field('hora_inicio',$p->ID);
        $p->hora_fin = get_field('hora_fin',$p->ID);
        $p->ubicacion = get_field('ubicacion',$p->ID);

        $fi = substr($p->fecha_inicio,6,4)."-".substr($p->fecha_inicio,3,2)."-".substr($p->fecha_inicio,0,2)." 00:00:00.0";
        $fi = strtotime($fi);
        $ff = substr($p->fecha_fin,6,4)."-".substr($p->fecha_fin,3,2)."-".substr($p->fecha_fin,0,2)." 00:00:00.0";
        $ff = strtotime($ff);
        
        $p->fi = $fi;
        $p->ff =  $ff;
        
        $today = strtotime("today", time());
        
        // var_dump($fi,$today);
        // if($fi<=$today && $today<=$ff){
        if($fi>=$today){
            $pr[] = $p;
        }
    endforeach;

    usort($pr, "cmp");

	$ret = $pr;

	return rest_ensure_response( $ret );
}

function cmp($a, $b)
{
    if ($a->$fi == $b->fi) {
        return 0;
    }
    return ($a->fi < $b->fi) ? -1 : 1;
}


// Add shortcodes
function autoridad_shortcode($atts) {
    extract( shortcode_atts( array(
        'id' => 0
    ), $atts, 'autoridad' ) );


    $post = get_post($id);

    return wpautop($post->post_content);
}
add_shortcode('autoridad', 'autoridad_shortcode');

// Agregamos las columnas de autoridades shortcode
add_action( 'manage_autoridades_posts_columns','add_column');
function add_column( $columns ) {
    return array_merge( $columns, array( 'shortcode' => 'Shortcode' ) );
}
add_action( 'manage_autoridades_posts_custom_column',  'manage_column', 10,  2 );
function manage_column( $column, $post_id ) {
    switch( $column ) {
        case 'shortcode' :
            echo "[autoridad id=".$post_id."]";
            break;
    }
}

//Agregamos las columnas de inicio y fin de los eventos
add_action( 'manage_evento_posts_columns','add_column_evento');
function add_column_evento( $columns ) {
    return array_merge( $columns, array( 'inicio' => 'Inicio', 'fin' => 'Fin' ) );
}
add_action( 'manage_evento_posts_custom_column',  'manage_column_evento', 10,  2 );
function manage_column_evento( $column, $post_id ) {
    switch( $column ) {
        case 'inicio' :
            $f = get_field('fecha_inicio',$post_id);
            echo substr($f,6,2)."/".substr($f,4,2)."/".substr($f,0,4);
            break;
        case 'fin' :
            $f = get_field('fecha_fin',$post_id);
            echo substr($f,6,2)."/".substr($f,4,2)."/".substr($f,0,4);
            break;
    }
}
