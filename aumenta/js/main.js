var $grid = null;
var qsRegex;
//Variables para anuncios
var url_site = "";
var url = "https://web.uvg.gt/anuncios/";
var referal = window.location.href.toString().split(window.location.host)[1];
var fu = window.location.href.toString();
jQuery(document).ready(function ($) {
    $('#q-buttons a').on("click", function () {
        $(".quick-menus,.quick-menus .qmenu ul").removeClass("active");
        $(".quick-menus,.quick-menus #" + $(this).data("id") + " ul").addClass("active");

        //Remove active menu
        $('#q-buttons a').removeClass("active");
        $(this).addClass("active");
        $("#page-container").css(marginTop, "-1px");
    });

    //Ponemos el mismo alto los cuadros de investigaciones
    var hi = 0;
    $.each($("#centros .et_pb_row .et_pb_module"), function (i, l) {
        console.log($(this).parent().parent().hasClass("et_pb_row_10"));
        if (hi < $(this).outerHeight()) {
            hi = $(this).outerHeight();
        }
    });
    $(".et_pb_module", $("#centros .et_pb_row").not(".et_pb_row_10")).outerHeight(hi);

    $.each($(".qmenu"), function (i, l) {
        var quantity = $("ul.menu > li", $(this)).length;
        $("ul.menu > li", $(this)).outerWidth(((1 / quantity) * 100) + "%");
    });

    $(".quick-menus .menu_close").on("click", function () {
        $(".quick-menus,.quick-menus .qmenu ul").removeClass("active");
        //Remove active menu
        $('#q-buttons a').removeClass("active");
    });

    $grid = $('.entries').isotope({
        // options
        itemSelector: '.element',
        layoutMode: 'fitRows',
        fitRows: {
            gutter: 10
        },
        filter: function () {
            return qsRegex ? $(this).text().match(qsRegex) : true;
        }
    });

    $('.filters li a').on('click', function () {
        $('.directorio-container .subtitle').html($(this).text());
        $grid.isotope({ filter: '.cat' + $(this).data("id") });
        $('.filters li a').removeClass("active");
        $(this).addClass("active");
    });
    $('#search').keyup(function (e) {
        qsRegex = new RegExp($(this).val(), 'gi');
        $('.directorio-container .subtitle').html('Buscando "' + $("#search").val() + '"');
        $grid.isotope();
        $('.filters li a').removeClass("active");
    });

    //Add images to the main menu
    $("#menu-principal > li:nth-child(1) > ul").prepend("<li class='image'>" + $("#wd-nosotros .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(1) > ul").prepend("<li class='right'>" + $("#wd-nosotros2 .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(2) > ul").prepend("<li class='proxima'>" + $("#wd-admisiones .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(2) > ul").prepend("<li class='right'>" + $("#wd-admisiones2 .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(4) > ul").prepend("<li class='image'>" + $("#wd-vida .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(4) > ul").prepend("<li class='right'>" + $("#wd-vida2 .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(5) > ul").prepend("<li class='image'>" + $("#wd-impacto .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(5) > ul").prepend("<li class='right'>" + $("#wd-impacto2 .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(6) > ul").prepend("<li class='image'>" + $("#wd-investigacion .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(6) > ul").prepend("<li class='right'>" + $("#wd-investigacion2 .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(7) > ul").prepend("<li class='image'>" + $("#wd-servicios .textwidget").html() + "</li>");
    $("#menu-principal > li:nth-child(7) > ul").prepend("<li class='right'>" + $("#wd-servicios2 .textwidget").html() + "</li>");

    //Add the academico menu under it's place
    $("#menu-principal > li:nth-child(3) > ul").html($("#menu-academico").html());
    // $("#menu-principal-1 > li:nth-child(3) > ul").html($("#menu-por-facultad").html());
    // $("#menu-principal-1 > li:nth-child(3) > ul > li").on('click', function (e) { e.preventDefault(); e.stopPropagation(); });
    if ($("body.home,body.page-id-7364").length > 0) {
        $.get('https://noticias.uvg.gt/wp-json/featured/v1/featured', function (data) {
            // console.log(data);
            var cont = "<ul>";
            for (var i = 0; i < data.length; i++) {
                var cats = "";
                for (var j = 0; j < data[i].cat.length; j++) {
                    cats += "cat" + data[i].cat[j].term_id + " ";
                }
                cont += "<li rel='noticias'>";
                cont += "<div class='cats'>" + data[i].cat_text + "</div>";
                cont += "<span class='img'> <img src='" + data[i].thumb + "'></span>";
                // cont += "<div class='title " + cats + "'>" + data[i].post_title.substr(0, 55) + "</div>";
                cont += "<div class='title " + cats + "'>" + data[i].post_title + "</div>";
                cont += "<div class='news-block'>";
                cont += "<div class='title'>" + data[i].post_title + "</div>";
                cont += "<div class='img'><img src='" + data[i].thumb + "'></div>";
                cont += "<div class='text'>" + data[i].post_content.replace(/<\/?[^>]+(>|$)/g, "") + "</div>";
                cont += "<div class='more'><a href='" + data[i].link + "' target='_blank'><i class='fa fa-external-link' aria-hidden='true'></i>leer más </a></div>";
                cont += "</div>";
                cont += "</li>";
            }
            cont += "</ul>";

            $(".noticias-home").html(cont);

            $.each($(".noticias-home ul li"), function (i, l) {
                var e = $(".news-block", $(this));
                $(this).colorbox({ inline: true, rel: "noticias", href: e, maxWidth: "90%", maxHeight: "90%" });
            });
            home_page_news_fix();
        });
    }

    $.each($("a").not(".buttonplan"), function (index, value) {
        // console.log(this);
        // Match elements containg youtube urls
        var st = $(this).attr("href");
        if (st !== "http://www.youtube.com/user/MediaUVG") {
            if (st) {
                var match = st.match(/(http:|https:)?(\/\/)?(www\.)?(youtube.com|youtu.be)\/(watch|embed)?(\?v=|\/)?(\S+)?/);
                if (match != null) {
                    st = st.replace("watch?v=", "embed/");
                    $(this).attr("href", st);
                    // console.log(match);
                    $(this).colorbox({ iframe: true, innerWidth: 640, innerHeight: 390 });
                    // Add matched URL to results
                }
            }
        }
    });

    //We load the egresados
    $.each($('.egresados-listado'), function (index, value) {
        //console.log($(this).data("id"));
        var current = $(this);
        $.get(
            url_site + '/wp-json/egresados/v1/get',
            {
                filter: $(this).data("id")
            },
            function (data) {
                var c = "<ul>";
                for (var i = 0; i < data.length; i++) {
                    c += "<li class='name" + data[i].cat + "'>";
                    c += "<a href='" + data[i].link + "'>";
                    c += "<img src='" + data[i].foto + "'>";
                    c += "<span class='name'>" + data[i].post_title + "</span>";
                    c += "</a>";
                    c += "</li>";
                }
                c += "</ul>";
                $(current).html(c);

                $.each($("a").not(".buttonplan"), function (index, value) {
                    // console.log(this);
                    // Match elements containg youtube urls
                    var st = $(this).attr("href");
                    if (st !== "http://www.youtube.com/user/MediaUVG") {
                        if (st) {
                            var match = st.match(/(http:|https:)?(\/\/)?(www\.)?(youtube.com|youtu.be)\/(watch|embed)?(\?v=|\/)?(\S+)?/);
                            if (match != null) {
                                st = st.replace("watch?v=", "embed/");
                                $(this).attr("href", st);
                                // console.log(match);
                                $(this).colorbox({ iframe: true, innerWidth: 640, innerHeight: 390 });
                                // Add matched URL to results
                            }
                        }
                    }
                });
            }
        );
    })

    //Becas line adjustment
    $.each($(".item-carrera-facultad"), function (index, value) {
        var h = $("img", $(this)).outerHeight();
        h = h - 37;
        $('h3', $(this)).css("top", h);
    });

    //Funciones para el menú de Académico
    $('.por-grado-click').on('click', function () {
        $('.por-grado-click,.por-facultad-click').removeClass("active");
        $(this).addClass("active");

        $('.col2,.col3').addClass("hide");
        $('.por-facultad').removeClass("active");
        $('.por-grado').addClass("active");
    });
    $('.menu-por-grado-container > .menu > li').on('click', function () {
        $('.menu-por-grado-container > .menu > li').removeClass("active");
        $(this).addClass("active");

        $(".menu-por-grado-container > .menu > li > ul").hide();
        var obj = $(this);
        $("ul", $(this)).show(function () {
            var hm = $("ul:first", $(obj)).outerHeight();
            $('.academico').removeAttr("style");
            $(obj).parent().removeAttr("style");
            if (hm > 217) {
                $('.academico').outerHeight(hm + 75);
                $(obj).parent().outerHeight(hm);
            }
        });
    });
    //Funciones para por facultad
    $('.por-facultad-click').on('click', function () {
        $('.por-grado-click,.por-facultad-click').removeClass("active");
        $(this).addClass("active");

        $('.col2,.col3').addClass("hide");
        $('.por-grado').removeClass("active");
        $('.por-facultad').addClass("active");
    });
    $('.menu-por-facultad-container > .menu > li').on('click', function () {
        $('.menu-por-facultad-container > .menu > li').removeClass("active");
        $(this).addClass("active");

        $(".menu-por-facultad-container > .menu > li > ul").hide();
        var obj = $(this);
        $("ul", $(this)).show(function () {
            var hm = $("ul:first", $(obj)).outerHeight();
            $('.academico').removeAttr("style");
            $(obj).parent().removeAttr("style");
            if (hm > 217) {
                $('.academico').outerHeight(hm + 75);
                if (hm > 315) {
                    $(obj).parent().outerHeight(hm);
                }
            }
        });
    });
    //Funciones para el menú de Académico

    //Funciones para el menú mobil
    $('.main-menu-mobile .bread').on('click', function () {
        $("ul:first", $(this).parent()).toggleClass("active");
    });
    $('.main-menu-mobile ul li.menu-item-has-children > a').attr("href", "#");
    $('.main-menu-mobile ul li.menu-item-has-children').on('click', function (e) {
        // console.log($(this));
        // e.preventDefault();
        // e.stopPropagation();
        if ($(this).parent().hasClass("et-show-dropdown")) {
            $(this).parent().removeClass("et-show-dropdown");
        }
    });
    //Funciones para el menú mobil

    //Título de color
    resize_titulo_color();
    //Título de color

    //Funcion el slider principal
    if ($("body.home").length > 0) {
        $.post(url + "api/anuncios/0?u=" + referal + "&fu=" + fu, function (data) { });

        $("#slider-home .et_pb_slide").on('click', function () {
            $.post(url + "api/count_link/0?u=" + referal + "&fu=" + fu, function (data) { });
        });
    }
    //Funcion el slider admisiones
    if ($("body.postid-2166").length > 0) {
        $.post(url + "api/anuncios/9998?u=" + referal + "&fu=" + fu, function (data) { });

        $("#slider-home .et_pb_slide").on('click', function () {
            $.post(url + "api/count_link/9998?u=" + referal + "&fu=" + fu, function (data) { });
        });
    }
    //Funcion el slider de vida estudiantil
    if ($("body.postid-4905").length > 0) {
        $.post(url + "api/anuncios/9999?u=" + referal + "&fu=" + fu, function (data) { });

        $("#slider-home .et_pb_slide").on('click', function () {
            $.post(url + "api/count_link/9999?u=" + referal + "&fu=" + fu, function (data) { });
        });
    }

    //Funcion para los anuncios
    $.each($(".banner-uvg-anuncios"), function (i, l) {
        var obj = $(this);
        $.post(url + "api/anuncios/" + $(this).data("id") + "?u=" + referal + "&fu=" + fu, function (data) {
            var mobile = 0;
            if (data.banner.length > 0) {
                info = "<div class='banner--anuncios--uvg'><div class='nivoSlider desktop'>";
                for (var i = 0; i < data.banner.length; i++) {
                    if (data.banner[i].img2.trim() !== "") {
                        mobile = 1;
                    }
                    if (data.banner[i].caption !== null) {
                        if (data.banner[i].caption !== "" && !data.banner[i].caption.match(/^[a-zA-Z]+:\/\//)) {
                            data.banner[i].caption = 'http://' + data.banner[i].caption;
                        }
                    }
                    var target = "";
                    var cl = "";
                    var url2 = url + "api/count_link/" + $(obj).data("id") + "?url=" + data.banner[i].caption + "&u=" + referal + "&fu=" + fu + "&name=" + data.name;
                    if (data.banner[i].blank == "blank") {
                        target = "_blank";
                    }
                    if (data.banner[i].blank == "lightbox") {
                        cl = "lbox";
                        url2 = data.banner[i].caption;
                    }

                    if (data.banner[i].caption !== "" && data.banner[i].caption !== null) info += "<a href='" + url2 + "' target='" + target + "' class='" + cl + "'>"; else info += "<span>";

                    info += "<img class='desktop' src='" + data.banner[i].img + "' />";

                    if (data.banner[i].caption !== "" && data.banner[i].caption !== null) info += "</a>"; else info += "</span>";
                }
                info += "</div></div>";

                $(obj).html(info);
                $(".nivoSlider").nivoSlider({ effect: "fade", controlNav: false });
            }

            //Mobile banners
            if (mobile === 1) {
                info = "<div class='banner--anuncios--uvg'><div class='nivoSliderMobile mobile'>";
                for (var i = 0; i < data.banner.length; i++) {
                    if (data.banner[i].img2.trim() !== "") {
                        if (data.banner[i].caption !== null) {
                            if (data.banner[i].caption !== "" && !data.banner[i].caption.match(/^[a-zA-Z]+:\/\//)) {
                                data.banner[i].caption = 'http://' + data.banner[i].caption;
                            }
                        }
                        var target = "";
                        var cl = "";
                        var url2 = url + "api/count_link/" + $(obj).data("id") + "?url=" + data.banner[i].caption + "&u=" + referal + "&fu=" + fu + "&name=" + data.name;
                        if (data.banner[i].blank == "blank") {
                            target = "_blank";
                        }
                        if (data.banner[i].blank == "lightbox") {
                            cl = "lbox";
                            url2 = data.banner[i].caption;
                        }
                        if (data.banner[i].caption !== "" && data.banner[i].caption !== null) info += "<a href='" + url2 + "' target='" + target + "' class='" + cl + "'>"; else info += "<span>";

                        info += "<img class='desktop' src='" + data.banner[i].img2 + "' />";

                        if (data.banner[i].caption !== "" && data.banner[i].caption !== null) info += "</a>"; else info += "</span>";
                    }
                }
                info += "</div></div>";

                $(obj).append(info);
                $(".nivoSliderMobile").nivoSlider({ effect: "fade", controlNav: false });
            }

            $(".lbox").colorbox({ iframe: true, width: "90%", height: "90%" });

        });
    });
    //Funcion para los anuncios

    //Modal del active directory para las carreras
    $(".intro-carrera a:last-child").colorbox({ iframe: true, width: "70%", height: "90%" });
    $(".modal-suscripcion").colorbox({ iframe: true, width: "70%", height: "90%", rel: "nofollow" });

    $(document).bind('cbox_complete', function (e) {
        var ww = jQuery(window).width();
        if (ww <= 650) {
            jQuery.fn.colorbox.resize({ innerWidth: innerWidth - 40, innerHeight: innerHeight - 40 });
        }
    });


    var m = Array("ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    //Eventos Home
    if ($(".eventos-avisos-home").length > 0) {
        $.get(
            url_site + '/wp-json/eventos/v1/get',
            function (data) {
                if (data.length === 0) {
                    $('.eventos-avisos-home .tabs .eventos').css("opacity", 0);
                }

                var d = "<ul>";
                var n = 0;
                for (var i = 0; i < data.length; i++) {

                    var varDate = new Date(data[i].fecha_inicio.substr(0, 4) + "-" + data[i].fecha_inicio.substr(4, 2) + "-" + data[i].fecha_inicio.substr(6, 2)); //dd-mm-YYYY
                    var varDate2 = new Date(data[i].fecha_fin.substr(0, 4) + "-" + data[i].fecha_fin.substr(4, 2) + "-" + data[i].fecha_fin.substr(6, 2)); //dd-mm-YYYY
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);

                    if (varDate >= today || (data[i].fecha_fin != "" && varDate2 >= today)) {
                        d += "<li>";
                        if (++n > 3) n = 1;
                        d += "<div class='date n" + n + "'>";
                        d += "<div class='ini'>";
                        d += "<div class='big'>";
                        d += data[i].fecha_inicio.substr(6, 2);
                        d += "</div>";
                        d += "<div class='small'>";
                        d += m[parseInt(data[i].fecha_inicio.substr(4, 2)) - 1];
                        d += "</div>";
                        d += "</div>";
                        d += "</div>";

                        d += "<div class='info'>";
                        d += "<div class='title'>" + data[i].post_title + "</div>";
                        d += "<div class='ubicacion'>" + data[i].ubicacion + "</div>";
                        d += "<div class='hora'>" + data[i].hora_inicio + "-" + data[i].hora_fin + "</div>";
                        if (data[i].enlace !== "") {
                            d += "<a href='" + data[i].enlace + "' class='enlace' target='_blank'>" + data[i].titulo_enlace + "</a>";
                        }
                        if (data[i].enlace_ver_mas !== "") {
                            d += "<a href='" + data[i].enlace_ver_mas + "' class='listados' target='_blank'><span>" + data[i].titulo_enlace_ver_mas + "</span><span class='mas'>+</span></a>";
                        }
                        d += "</div>";
                        d += "</li>";
                    }
                }

                d += "</ul>";

                $(".eventos-avisos-home .contents .eventos").html(d);
                $(".eventos-avisos-home .contents .eventos ul").slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1300,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 991,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });

                //Ponemos el mismo alto a los slides
                var h = 0;
                $.each($(".slick-slide"), function (i, l) {
                    if (h < $(this).outerHeight()) {
                        h = $(this).outerHeight();
                    }
                });
                $(".slick-slide").outerHeight(h);
            }
        );
        //Avisos Home
        $.get(
            url_site + '/wp-json/avisos/v1/get',
            function (data) {
                if (data.length === 0) {
                    $('.eventos-avisos-home .tabs .avisos').css("opacity", 0);
                }
                var d = "<ul>";
                var n = 0;
                for (var i = 0; i < data.length; i++) {
                    var varDate = "";
                    var varDate2 = "";
                    if (data[i].fecha_inicio !== null) {
                        varDate = new Date(data[i].fecha_inicio.substr(6, 4) + "-" + data[i].fecha_inicio.substr(3, 2) + "-" + data[i].fecha_inicio.substr(0, 2));
                        // console.log(varDate);
                    }
                    if (data[i].fecha_fin !== null) {
                        varDate2 = new Date(data[i].fecha_fin.substr(6, 4) + "-" + data[i].fecha_fin.substr(3, 2) + "-" + data[i].fecha_fin.substr(0, 2));
                    }
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);

                    // console.log(today, varDate, varDate2);
                    if (varDate >= today || (data[i].fecha_fin != "" && varDate2 >= today)) {
                        d += "<li>";
                        if (++n > 3) n = 1;
                        d += "<div class='date n" + n + "'>";
                        d += "<div class='ini'>";
                        d += "<div class='big'>";
                        d += data[i].fecha_inicio.substr(0, 2);
                        d += "</div>";
                        d += "<div class='small'>";
                        d += m[parseInt(data[i].fecha_inicio.substr(3, 2)) - 1];
                        d += "</div>";
                        d += "</div>";
                        d += "</div>";

                        d += "<div class='info'>";
                        d += "<div class='title'>" + data[i].post_title + "</div>";
                        d += "<div class='ubicacion'>" + data[i].ubicacion + "</div>";
                        d += "<div class='hora'>" + data[i].hora_inicio;
                        if (data[i].hora_fin !== "") {
                            d += " - " + data[i].hora_fin + "</div>";
                        }
                        if (data[i].enlace_externo !== "") {
                            d += "<a href='" + data[i].enlace_externo + "' class='listados' target='_blank'><span>" + data[i].titulo_enlace_ver_mas + "</span><span class='mas'>+</span></a>";
                        }
                        d += "</div>";
                        d += "</li>";

                    }
                }

                d += "</ul>";

                $('.eventos-avisos-home .contents .avisos').html(d);
                $(".eventos-avisos-home .contents .avisos ul").slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 991,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
                // window.setTimeout($(".eventos-avisos-home .tabs .avisos").click(), 5000);
            }
        );
        //Click en tabs de avisos y eventos
        $(".eventos-avisos-home .tabs .eventos").on("click", function () {
            $(this).addClass("active");
            $(".eventos-avisos-home .contents .eventos").addClass("active");
            $(".eventos-avisos-home .contents .avisos").removeClass("active");
            $(".eventos-avisos-home .tabs .avisos").removeClass("active");
            //Fix issue with slick being hidden
            $('.eventos-avisos-home .contents .eventos ul').slick('setPosition');

        });
        $(".eventos-avisos-home .tabs .avisos").on("click", function () {
            $(this).addClass("active");
            $(".eventos-avisos-home .contents .avisos").addClass("active");
            $(".eventos-avisos-home .tabs .eventos").removeClass("active");
            $(".eventos-avisos-home .contents .eventos").removeClass("active");
            //Fix issue with slick being hidden
            $('.eventos-avisos-home .contents .avisos ul').slick('setPosition');
        });
    }
    //Eventos Home

    //Submenus
    $('.menu-ancho-completo .et_pb_row_fullwidth .menu-secundario-full ul').parent().prepend('<i class="fa fa-bars hamburguer-full" aria-hidden="true"></i>');
    //On click submenu
    $('.menu-ancho-completo .et_pb_row_fullwidth .menu-secundario-full').on('click', '.hamburguer-full', function () {
        $("ul", $(this).parent()).toggleClass("active");
    });
    //Submenu Carreras
    $('.menu-secundario ul').parent().prepend('<i class="fa fa-bars hamburguer-secondary" aria-hidden="true"></i>');
    //On click submenu
    $('.menu-secundario').on('click', '.hamburguer-secondary', function () {
        $("ul", $(this).parent()).toggleClass("active");
    });

    //Remove the download attribute from the folleto
    $(".buttonplan").removeAttr("download");//.removeAttr("target");

    //Make the link active on click
    $('.menu-secundario ul li a').on('click', function () {
        $('.menu-secundario ul li a').removeClass("active");
        $(this).addClass('active');
    });

    //Make the submenu link active depeding on the window href
    var current_url = document.location.href;
    $.each($(".menu-secundario-full ul li a"), function (i, l) {
        if ($(this).attr("href") == current_url) {
            $(this).addClass("active");
        }
    });

    // Menús automáticos
    var cur_loc = window.location.href;
    var c = cur_loc.split("/")[3];
    if (c == "academico" || c == "facultades" || c == "carreras") {
        $("#menu-principal > li:nth-of-type(3)").addClass("active");
    } else {
        var match = 0;
        $.each($("#menu-principal > li"), function (i, l) {
            $.each($("ul.sub-menu:nth-of-type(1) > li:nth-of-type(3) > ul > li > a", $(l)), function () {
                if (cur_loc == $(this).attr("href")) {
                    match = 1;
                    $(this).parent().parent().parent().parent().parent().addClass("active");
                    return false;
                }
            });
            if (match === 1) {
                return false;
            }
        });
    }


    //End Document Ready
});

jQuery(window).resize(function ($) {
    resize_titulo_color();
    //Becas line adjustment
    jQuery.each(jQuery(".item-carrera-facultad"), function (index, value) {
        var h = jQuery("img", jQuery(this)).outerHeight();
        h = h - 37;
        jQuery('h3', jQuery(this)).css("top", h);
    });
});

function resize_titulo_color() {
    jQuery(".modulo-titulo-color h2").removeAttr("style");
    jQuery.each(jQuery(".modulo-titulo-color"), function (i, l) {
        var h = 0;
        jQuery.each(jQuery(".modulo-titulo-color h2", jQuery(this).parent().parent()), function (i, l) {
            if (h < jQuery(this).outerHeight()) {
                h = jQuery(this).outerHeight();
            }
        });
        jQuery(".modulo-titulo-color h2", jQuery(this).parent().parent()).removeAttr("style").outerHeight(h);


        var h1 = 0
        //Obtenemos el más sin el header
        jQuery.each(jQuery(".modulo-titulo-color", jQuery(this).parent().parent()), function (i, l) {
            if (h1 < jQuery(this).outerHeight() - h) {
                h1 = jQuery(this).outerHeight() - h;
            }
        });

        //Actualizamos el tamaño del último p
        jQuery.each(jQuery(".modulo-titulo-color", jQuery(this).parent().parent()), function (i, l) {
            var h2 = jQuery(this).outerHeight() - h;
            var lastP = jQuery("p, ul", jQuery(this)).last().outerHeight();

            // console.log(jQuery("p,ul", jQuery(this)).last(), h2, h1, lastP);
            // console.log(lastP + h1 - h2);
            if (h2 < h1) {
                jQuery("p,ul", jQuery(this)).last().outerHeight(lastP + h1 - h2);
            }
            // if (h1 < jQuery(this).outerHeight() - h) {
            //     h1 = jQuery(this).outerHeight() - h;
            // }
        });

        // jQuery.each(jQuery(".modulo-titulo-color p,.modulo-titulo-color ul", jQuery(this).parent().parent()), function (i, l) {
        //     if (h1 < jQuery(this).outerHeight() - h) {
        //         h1 = jQuery(this).outerHeight() - h;
        //     }
        // });

        // jQuery(".modulo-titulo-color p,.modulo-titulo-color ul", jQuery(this).parent().parent()).removeAttr("style").outerHeight(h);
    });

    jQuery.each(jQuery(".directorio-container .entries .element"), function (i, l) {
        //El título
        var h = 0;
        jQuery.each(jQuery("h2", jQuery(this).parent()), function (i, l) {
            if (h < jQuery(this).outerHeight()) {
                h = jQuery(this).outerHeight();
            }
        });
        jQuery(".element h2", jQuery(this).parent()).css("height", "").outerHeight(h);
        //El bloque gris
        h = 0;
        jQuery.each(jQuery(".text", jQuery(this).parent()), function (i, l) {
            if (h < jQuery(this).outerHeight()) {
                h = jQuery(this).outerHeight();
            }
        });
        jQuery(".element .text", jQuery(this).parent()).removeAttr("style").outerHeight(h);

        $grid.isotope('layout')

    });
    home_page_news_fix();
}
function home_page_news_fix() {
    //Home page news title fix
    var h1 = 0;
    jQuery(".noticias-home ul li .title").removeAttr("style");
    jQuery.each(jQuery(".title", jQuery(".noticias-home ul li")), function (i, l) {
        if (h1 < jQuery(this).outerHeight()) {
            h1 = jQuery(this).outerHeight();
        }
    });
    // console.log(h1);

    jQuery(".noticias-home ul li .title").removeAttr("style").outerHeight(h1);
}
// When the user scrolls the page, execute myFunction 
window.onscroll = function () { myFunction() };

// Get the header
var header = document.getElementsByClassName("menu-ancho-completo")[0];
var header2 = document.getElementsByClassName("menu-secundario")[0];

// Get the offset position of the navbar
var sticky, sticky2;
if (typeof header !== "undefined")
    sticky = header.offsetTop;
if (typeof header2 !== "undefined") {
    sticky2 = header2.offsetTop;
    header2.parentNode.parentNode.style.zIndex = 99;
}

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (typeof header !== 'undefined') {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

    //Carreras
    if (typeof header2 !== 'undefined') {
        if (window.pageYOffset > sticky2) {
            header2.classList.add("sticky");
        } else {
            header2.classList.remove("sticky");
        }
    }

}

// All list items
var menuItems = jQuery(".menu-secundario ul li a");
// Anchors corresponding to menu items
var scrollItems = menuItems.map(function () {
    var item = jQuery(jQuery(this).attr("href"));
    if (item.length) { return item; }
});
jQuery(window).scroll(function () {
    if (menuItems.length > 0) {
        // Get container scroll position
        var fromTop = jQuery(this).scrollTop() + 140;

        // Get id of current scroll item
        var cur = scrollItems.map(function () {
            if (jQuery(this).offset().top < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";
        // console.log(id);
        // Set/remove active class
        menuItems.removeClass("active").filter("[href='#" + id + "']").addClass("active");
    }
});
