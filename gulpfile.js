var gulp = require("gulp");
var livereload = require("gulp-livereload");
var uglify = require("gulp-uglifyjs");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var sourcemaps = require("gulp-sourcemaps");
var imagemin = require("gulp-imagemin");
var pngquant = require("imagemin-pngquant");
var notify = require("gulp-notify");

gulp.task("imagemin", function () {
  return gulp
    .src("./themes/custom/lcp_cpp/images/*")
    .pipe(
      imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        use: [pngquant()]
      })
    )
    .pipe(gulp.dest("./themes/custom/lcp_cpp/images"));
});

gulp.task("sass", function () {
  gulp
    .src("./aumenta/scss/**/*.scss")
    .pipe(sourcemaps.init())
    // .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sass({ outputStyle: "compact" }).on("error", sass.logError))
    .pipe(
      autoprefixer(
        "last 2 version",
        "safari 5",
        "ie 7",
        "ie 8",
        "ie 9",
        "opera 12.1",
        "ios 6",
        "android 4"
      )
    )
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./aumenta/"))
    .pipe(notify({ message: "Sass task complete" }));
});

gulp.task("uglify", function () {
  gulp
    .src("./themes/custom/lcp_cpp/lib/*.js")
    .pipe(uglify("script.js"))
    .pipe(gulp.dest("./themes/custom/lcp_cpp/js"))
    .pipe(notify({ message: "Scripts task complete" }));
});

gulp.task("watch", function () {
  // livereload.listen();

  gulp.watch("./aumenta/scss/**/*.scss", ["sass"]);
  // gulp.watch('./themes/custom/lcp_cpp/lib/*.js', ['uglify']);
  // gulp.watch(['./themes/custom/lcp_cpp/css/style.css', './themes/custom/lcp_cpp/**/*.twig', './themes/custom/lcp_cpp/js/*.js'], function (files){
  //     livereload.changed(files)
  // });
});
